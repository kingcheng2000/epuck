#include <p30F6014A.h>
#include <motor_led/e_epuck_ports.h>
#include <motor_led/e_init_port.h>
#include <a_d/advance_ad_scan/e_prox.h>
#include <a_d/advance_ad_scan/e_ad_conv.h>
#include <uart/e_uart_char.h>
#include <stdio.h>
#include <codec/e_sound.h>
#include <led.h>


void Wait(long);
int GetSelector(void);



int main()
{
    // Initialisation. 
    e_init_port(); // Initialises the ports.
    e_init_ad_scan(ALL_ADC); // Initialises the analogue to digital converters.
    e_led_clear(); // Turn off all the LEDs in the initialisation stage.
    e_start_agendas_processing(); // Allows the robot to execute multiple tasks.
    
    e_init_uart1(); // Initialises the UART. Uncomment when you use Bluetooth communication.
    e_calibrate_ir(); // Uncomment when you use Proximity Sensors.
    e_init_motors(); // Initialises the motors. Uncomment when you use the motors.
	e_init_sound();
    Wait(4000000);
    // Main Loop.
	int temp_max = e_get_prox(0);
	int n = 0;
	int m = 0;
	int i = 0;
	int randomness = 1;

while(1)
{

			
	if (GetSelector() == 4) 
	{
		int motor_speed = 700;
		int motor_speed_reverse = -700;
		int motor_speed_zero = 0;

		
						//narrow passages
			if (e_get_prox(0) > 250 && e_get_prox(7) > 250) //test extra condition
				{  
					e_set_speed_right(motor_speed);
					e_set_speed_left(motor_speed_reverse);
					SetLed (0,1);
					Wait(500000);         
				}  
			else if (e_get_prox(0)>250 || e_get_prox(1)>250 || e_get_prox(6)>250 || e_get_prox(7)>250 ){
			e_set_speed_right(motor_speed_zero);
			e_set_speed_left(motor_speed_zero);
			Wait(200000);
				//corners
				if (e_get_prox(5) > 250 || e_get_prox(6) > 250 || e_get_prox(7) > 250) //test extra condition
				{  //turn left, right detection
					e_set_speed_right(motor_speed);
					e_set_speed_left(motor_speed_reverse);
					Wait(200000);     
				}  
				else if(e_get_prox(0) > 250 || e_get_prox(1) > 250 || e_get_prox(2) > 250) //test extra condition
				{  
					e_set_speed_left(motor_speed);
					e_set_speed_right(motor_speed_reverse);
					Wait(900000);     
				}

			} else{
				
				e_set_speed_right(motor_speed);
				e_set_speed_left(motor_speed);
			
			}

			if (randomness == 50000){
				e_set_speed_right(motor_speed);
				e_set_speed_left(motor_speed_reverse);	
				Wait(1000000); 
				randomness = 1;
			}
			
			randomness = randomness + 1;

	}

	else
	{
	int motor_reverse = -500;
	int motor = 500;


	   while (n < 7)
		{
		  if(e_get_prox(n) > temp_max)
			{
			  temp_max = e_get_prox(n);
			  m = n; // index of proximity sensor with highest value
			}
			n = n + 1;
		}
		if (i == m)
		{
			if (temp_max > 300 && !(m == 4 || m == 3))
				{
				e_set_speed_right(-1000);
				e_set_speed_left(-1000);
				Wait(200000);// Wait values need to be tweaked        
				}
			else if (temp_max <= 250 && temp_max  > 50)
			{
				e_set_speed_right(motor);
				e_set_speed_left(motor);
				Wait(200000);// Wait values need to be tweaked
			}
		}
		else if(m == 1)
		{
		  e_set_speed_right(motor_reverse);
		  e_set_speed_left(motor);
		  e_set_led(1,1);
		  Wait(200000);// Wait values need to be tweaked
		  e_set_led(1,0);
		}
		else if(m == 2)
		{
		  e_set_speed_right(motor_reverse);
		  e_set_speed_left(motor);
		  e_set_led(2,1);
		  Wait(400000);
		  e_set_led(2,0);
		}
		else if(m == 3)
		{
		  e_set_speed_right(motor_reverse);
		  e_set_speed_left(motor);
		  e_set_led(3,1);
		  Wait(700000);
		  e_set_led(3,0);
		}
		else if(m == 4)
		{
		  e_set_speed_right(motor);
		  e_set_speed_left(motor_reverse);
		  e_set_led(5,1);
		  Wait(700000);
		  e_set_led(5,0);
		}
		else if(m == 5)
		{
		  e_set_speed_right(motor);
		  e_set_speed_left(motor_reverse);
		  e_set_led(6,1);
		  Wait(400000);
		  e_set_led(6,0);
		}
		else if(m == 6)
		{
		  e_set_speed_right(motor);
		  e_set_speed_left(motor_reverse);
		  e_set_led(7,1);
		  Wait(200000);
		  e_set_led(7,0);
		}
		else if (e_get_prox(0) < 20 && e_get_prox(1) < 20 && e_get_prox(3) < 20 && e_get_prox(4) < 20 && e_get_prox(5) < 20 && e_get_prox(6) < 20 && e_get_prox(7) < 20)  
		{
			e_set_speed_right(motor);
			e_set_speed_left(motor);
			e_play_sound(0, 2112); //makes sound when cant find epuck
			Wait(200000);
		}

		i = m;
		n = 0;
	}
}
return 0;
}

void Wait(long Duration) 
{
    long i;
    for(i=0;i<Duration;i++);
}

int GetSelector() {
    return SELECTOR0 + 2*SELECTOR1 + 4*SELECTOR2 + 8*SELECTOR3;
}